import matplotlib.pyplot as plt
import codecs

def graphic_of_relationship(path):
    file = codecs.open(path + u"/good_triple", "r", "utf8").read()
    distance = []
    rule_number = []
    layer = []

    triple = file.split("\n")
    for string in triple:
        tmp = string.split()
        if len(tmp)==3:
            distance.append(int(tmp[0]))
            rule_number.append(int(tmp[1]))
            layer.append(int(tmp[2]))

    file1 = codecs.open(path + u"/bad_triple", "r", "utf8").read()
    distance1 = []
    rule_number1 = []
    layer1 = []

    triple1 = file1.split("\n")
    for string in triple1:
        tmp = string.split()
        if len(tmp)==3:
            distance1.append(int(tmp[0]))
            rule_number1.append(int(tmp[1]))
            layer1.append(int(tmp[2]))

    max_good_distance = max(distance)
    max_good_layer = max(layer)
    max_good_rule_number = max(rule_number)

    min_good_distance = min(distance)
    min_good_layer = min(layer)
    min_good_rule_number = min(rule_number)

    max_bad_distance = max(distance1)
    max_bad_layer = max(layer1)
    max_bad_rule_number = max(rule_number1)

    min_bad_distance = min(distance1)
    min_bad_layer = min(layer1)
    min_bad_rule_number = min(rule_number1)


    plt.plot(layer, distance, 'ro', layer1, distance1, 'g^')
    plt.axis([min(min_good_layer, min_bad_layer) - 1, max(max_good_layer, max_bad_layer) + 1,
              min(min_good_distance, min_bad_distance) - 1, max(max_good_distance, max_bad_distance) + 1])
    plt.xlabel('Layer type')
    plt.ylabel('Distance')
    plt.savefig(path + '/layer_distance.png')
    plt.cla()
    plt.clf()

    plt.plot(rule_number, distance, 'ro', rule_number1, distance1, 'g^')
    plt.axis([min(min_good_rule_number, min_bad_rule_number) - 1, max(max_good_rule_number, max_bad_rule_number) + 1,
              min(min_good_distance, min_bad_distance) - 1, max(max_good_distance, max_bad_distance) + 1])
    plt.xlabel('CP rule number')
    plt.ylabel('Distance')
    plt.savefig(path + '/rule_number_distance.png')
    plt.cla()
    plt.clf()

    plt.plot(layer, rule_number, 'ro', layer1, rule_number1, 'g^')
    plt.axis([min(min_good_layer, min_bad_layer) - 1, max(max_good_layer, max_bad_layer) + 1,
              min(min_good_rule_number, min_bad_rule_number) - 1, max(max_good_rule_number, max_bad_rule_number) + 1])
    plt.xlabel('Layer type')
    plt.ylabel('CP rule number')
    plt.savefig(path + '/layer_rule_number.png')
    plt.cla()
    plt.clf()