import matplotlib.pyplot as plt
import codecs
from collections import defaultdict


def get_from_dict_two_lists(d):
    keys = []
    values = []

    for key, value in d.items():
        keys.append(key)
        values.append(value)

    return (keys, values)

def graphic_of_parameters(path):
    file = codecs.open(path + u"/good_triple", "r", "utf8").read()
    distance = []
    rule_number = []
    layer = []
    distance_dict = defaultdict(lambda :0)
    rule_number_dict = defaultdict(lambda :0)
    layer_dict = defaultdict(lambda :0)

    triple = file.split("\n")
    for string in triple:
        tmp = string.split()
        if len(tmp)==3:
            distance.append(int(tmp[0]))
            distance_dict[int(tmp[0])] += 1
            rule_number.append(int(tmp[1]))
            rule_number_dict[int(tmp[1])] += 1
            layer.append(int(tmp[2]))
            layer_dict[int(tmp[2])] += 1

    file1 = codecs.open(path + u"/bad_triple", "r", "utf8").read()
    distance1 = []
    rule_number1 = []
    layer1 = []
    distance_dict1 = defaultdict(lambda :0)
    rule_number_dict1 = defaultdict(lambda :0)
    layer_dict1 = defaultdict(lambda :0)

    triple1 = file1.split("\n")
    for string in triple1:
        tmp = string.split()
        if len(tmp)==3:
            distance1.append(int(tmp[0]))
            distance_dict1[int(tmp[0])] += 1
            rule_number1.append(int(tmp[1]))
            rule_number_dict1[int(tmp[1])] += 1
            layer1.append(int(tmp[2]))
            layer_dict1[int(tmp[2])] += 1

    #good
    distance = get_from_dict_two_lists(distance_dict)[0]
    distance_values = get_from_dict_two_lists(distance_dict)[1]
    layer = get_from_dict_two_lists(layer_dict)[0]
    layer_values = get_from_dict_two_lists(layer_dict)[1]
    rule_number = get_from_dict_two_lists(rule_number_dict)[0]
    rule_number_values = get_from_dict_two_lists(rule_number_dict)[1]

    #bad
    distance1 = get_from_dict_two_lists(distance_dict1)[0]
    distance1_values = get_from_dict_two_lists(distance_dict1)[1]
    layer1 = get_from_dict_two_lists(layer_dict1)[0]
    layer1_values = get_from_dict_two_lists(layer_dict1)[1]
    rule_number1 = get_from_dict_two_lists(rule_number_dict1)[0]
    rule_number1_values = get_from_dict_two_lists(rule_number_dict1)[1]

    max_good_distance = max(distance)
    max_good_layer = max(layer)
    max_good_rule_number = max(rule_number)
    max_good_distance_values = max(distance_values)
    max_good_layer_values = max(layer_values)
    max_good_rule_number_values = max(rule_number_values)

    min_good_distance = min(distance)
    min_good_layer = min(layer)
    min_good_rule_number = min(rule_number)
    min_good_distance_values = min(distance_values)
    min_good_layer_values = min(layer_values)
    min_good_rule_number_values = min(rule_number_values)

    max_bad_distance = max(distance1)
    max_bad_layer = max(layer1)
    max_bad_rule_number = max(rule_number1)
    max_bad_distance_values = max(distance1_values)
    max_bad_layer_values = max(layer1_values)
    max_bad_rule_number_values = max(rule_number1_values)

    min_bad_distance = min(distance1)
    min_bad_layer = min(layer1)
    min_bad_rule_number = min(rule_number1)
    min_bad_distance_values = min(distance1_values)
    min_bad_layer_values = min(layer1_values)
    min_bad_rule_number_values = min(rule_number1_values)

    #good
    plt.plot(distance, distance_values, 'ro')
    plt.yscale(u'log')
    plt.axis([min_good_distance - 1, max_good_distance + 1, min_good_distance_values - 1, max_good_distance_values + 1])
    plt.xlabel('Distance')
    plt.ylabel('Number of definitions')
    plt.savefig(path + '/good_distance.png')
    plt.cla()
    plt.clf()

    plt.plot(layer, layer_values, 'ro')
    plt.yscale(u'log')
    plt.axis([min_good_layer - 1, max_good_layer + 1, min_good_layer_values - 1, max_good_layer_values + 1])
    plt.xlabel('Type of layer')
    plt.ylabel('Number of definitions')
    plt.savefig(path + '/good_layer.png')
    plt.cla()
    plt.clf()

    plt.plot(rule_number, rule_number_values, 'ro')
    plt.yscale(u'log')
    plt.axis([min_good_rule_number - 1, max_good_rule_number + 1, min_good_rule_number_values - 1, max_good_rule_number_values + 1])
    plt.xlabel('CP rule number')
    plt.ylabel('Number of definitions')
    plt.savefig(path + '/good_rule_number.png')
    plt.cla()
    plt.clf()

    #bad
    plt.plot(distance1, distance1_values, 'ro')
    plt.yscale(u'log')
    plt.axis([min_bad_distance - 1, max_bad_distance + 1, min_bad_distance_values - 1, max_bad_distance_values + 1])
    plt.xlabel('Bad distance')
    plt.ylabel('Number of definitions')
    plt.savefig(path + '/bad_distance.png')
    plt.cla()
    plt.clf()

    plt.plot(layer1, layer1_values, 'ro')
    plt.yscale(u'log')
    plt.axis([min_bad_layer - 1, max_bad_layer + 1, min_bad_layer_values - 1, max_bad_layer_values + 1])
    plt.xlabel('Bad type of layer')
    plt.ylabel('Number of definitions')
    plt.savefig(path + '/bad_layer.png')
    plt.cla()
    plt.clf()

    plt.plot(rule_number1, rule_number1_values, 'ro')
    plt.yscale(u'log')
    plt.axis([min_bad_rule_number - 1, max_bad_rule_number + 1, min_bad_rule_number_values - 1, max_bad_rule_number_values + 1])
    plt.xlabel('Bad CP rule number')
    plt.ylabel('Number of definitions')
    plt.savefig(path + '/bad_rule_number.png')
    plt.cla()
    plt.clf()