# ScienceWISE abbreviation library

Description of the library: https://docs.google.com/document/d/19sdHMIyJ0-pznRnL2fWoLK_iftOXqPQl3OoioDUnOXk/edit

# Measuring performance of the library

```python
>> from swabbrlib.swabbrlib import get_abbr_with_definition
>> len(text) / 1024
331
>> get_abbr_with_definition(text)
{'BN': [u'Bloch-Nordsieck'],
 'HTL': [u'hard', u'thermal', u'loops'],
 'LLA': [u'leading', u'logarithmic', u'accuracy'],
 'LO': [u'leading-order'],
 'NLLA': [u'next-to-leading', u'logarithmic', u'accuracy'],
 'NLO': [u'next-to-leading', u'order']}
>> %timeit get_abbr_with_definition(text)
1 loop, best of 3: 216 ms per loop
```

i.e., for ~300 kB text with small amount of abbreviations, extraction proceeds at a speed of ~1.5 MB/sec