from setuptools import setup, find_packages
setup(
    name='swabbrlib',
    version='1.0',
    packages=find_packages(),
    install_requires=['simplejson'],
    package_data={'swabbrlib': ['blacklist', 'standard_rules.json']}
)
