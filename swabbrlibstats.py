import json
import codecs
import re
import os
import datetime
import csv
from nltk.stem.snowball import SnowballStemmer
from collections import defaultdict

from swabbrlib.swabbrlib import get_abbr_with_definition, get_best_rule, get_abbr_pattern, get_black_list, ORDINARY_WORD_TYPE, \
    BLACKLIST_WORD_TYPE, \
    abbr_pattern
from graphic import graphic_of_relationship
from graphic_alone import graphic_of_parameters

RULES_STATS_FILE_NAME = u"rules.json"
RULES_DIRECTORY = u"rules"
MIN_RULE_COUNT = 1
CLEAR_EQUALS = 0
LEVENSHTAIN_EQUALS = 1
NOT_EQUALS = 3
SUBSET_EQUALS = 2
stemmer = SnowballStemmer("english")

def write_to_excel(path, d, total_number_of_examples):
    with open(path + '/table.csv', 'w') as csvfile:
        fieldnames = [u'Number_of_letters_in_abbr', u'The_number_of_strict_matches', u'The_number_of_nested_matches',
                      u'The_number_of_matches_when_L_dist_<_2', u'No_matches', u'Total_matches']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter=';', lineterminator='\n')
        writer.writeheader()

        sum_type_0 = 0
        sum_type_1 = 0
        sum_type_2 = 0
        sum_type_3 = 0
        sum_total = 0

        for cnt_letters, dictionary in d.items():
            sum_type_0 += dictionary[0]
            sum_type_1 += dictionary[1]
            sum_type_2 +=  dictionary[2]
            sum_type_3 += dictionary[3]
            sum_total += dictionary[0] + dictionary[1] + dictionary[2]
            writer.writerow({u'Number_of_letters_in_abbr': cnt_letters,
                             u'The_number_of_strict_matches': dictionary[0],
                             u'The_number_of_nested_matches': dictionary[1],
                             u'The_number_of_matches_when_L_dist_<_2': dictionary[2],
                             u'No_matches': dictionary[3],
                             u'Total_matches': dictionary[0] + dictionary[1] + dictionary[2]})

        writer.writerow({u'Number_of_letters_in_abbr': u"Sum",
                             u'The_number_of_strict_matches': sum_type_0,
                             u'The_number_of_nested_matches': sum_type_1,
                             u'The_number_of_matches_when_L_dist_<_2': sum_type_2,
                             u'No_matches': sum_type_3,
                             u'Total_matches': sum_total})

        writer.writerow({u'Number_of_letters_in_abbr': u"Recall",
                             u'The_number_of_strict_matches': sum_type_0 * 100.0/total_number_of_examples,
                             u'The_number_of_nested_matches': sum_type_1 * 100.0/total_number_of_examples,
                             u'The_number_of_matches_when_L_dist_<_2': sum_type_2 * 100.0/total_number_of_examples,
                             u'No_matches': sum_type_3 * 100.0/total_number_of_examples,
                             u'Total_matches': sum_total * 100.0/total_number_of_examples})

        sum = sum_type_0 + sum_type_1 + sum_type_2 + sum_type_3
        writer.writerow({u'Number_of_letters_in_abbr': u"Precision",
                             u'The_number_of_strict_matches': sum_type_0 * 100.0 / sum,
                             u'The_number_of_nested_matches': sum_type_1 * 100.0 / sum,
                             u'The_number_of_matches_when_L_dist_<_2': sum_type_2 * 100.0 / sum,
                             u'No_matches': sum_type_3 * 100.0 / sum,
                             u'Total_matches': sum_total * 100.0 / sum})

def distance(a, b):
    "Calculates the Levenshtein distance between a and b."
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a, b = b, a
        n, m = m, n

    current_row = range(n+1) # Keep current and previous row, not entire matrix
    for i in range(1, m+1):
        previous_row, current_row = current_row, [i]+[0]*n
        for j in range(1,n+1):
            add, delete, change = previous_row[j]+1, current_row[j-1]+1, previous_row[j-1]
            if a[j-1] != b[i-1]:
                change += 1
            current_row[j] = min(add, delete, change)

    return current_row[n]


def abbr_pattern_and_candidate_patern_to_file_form(abbr_pattern, candidate_patern):
    return u"".join(abbr_pattern) + u":" + u"".join(candidate_patern)


def is_correct_pair_abbr_definition(abbr, definition):
    definition_words = definition.split()
    return (len(abbr) < len(definition) and min(len(abbr) + 5, len(abbr) * 2) >= len(definition_words) and abbr_pattern.match(abbr) and
            abbr.lower()[0] == definition.lower()[0] and definition.find(abbr) == -1)


def clear_data_for_build_rules(data):
    return [(abbr, definition, sentence) for abbr, definition, sentence in data if
            is_correct_pair_abbr_definition(abbr, definition)]


def prepare_base_rules(file_name):
    with open(file_name, 'r') as file:
        examples = json.loads(file.read())

    part = 0
    delta = 0.1
    clear_examples = clear_data_for_build_rules(examples)
    len_clear_examples = len(clear_examples)
    rules = defaultdict(lambda: defaultdict(lambda: 0))

    count_with_none = 0

    for i, (abbr, definition, _) in enumerate(clear_examples):
        if i * 1.0 / len_clear_examples > part:
            with open(RULES_DIRECTORY + u"/rules_" + str(part), 'w') as file:
                all_rules_to_add = {}
                for (abbr_patern, definition_type), rules_count in rules.items():
                    rules_to_add = [rule for rule, count in rules_count.items() if count > MIN_RULE_COUNT]
                    all_rules_to_add[
                        abbr_pattern_and_candidate_patern_to_file_form(abbr_patern, definition_type)] = rules_to_add
                file.write(json.dumps(all_rules_to_add))
            part += delta

        black_list = get_black_list()
        definition = tuple(definition.split())
        definition_type = tuple([BLACKLIST_WORD_TYPE if word in black_list else ORDINARY_WORD_TYPE
                                 for word in definition])
        rule, _ = get_best_rule(abbr, definition, definition_type)

        if rule is not None:
            rule = tuple(rule)
            rules[(tuple(get_abbr_pattern(abbr)), definition_type)][rule] += 1
        else:
            count_with_none += 1
            print abbr, definition

    print u"Not found rules:" + unicode(count_with_none) + u"/" + unicode(len_clear_examples)


def is_correct_for_statistics(abbr, definition, sentence):
    if not is_correct_pair_abbr_definition(abbr, definition):
        return False

    definition_words = definition.replace(u"-", u" ").split()

    for word in definition_words:
        if sentence.lower().find(word.lower()) == -1:
            return False
    return True

def is_correct_for_statistics_with_many_definitions(abbr, definitions, sentence):
    one_of_definitions_in_sentence = False
    one_of_definitions_is_correct = False

    for definition in definitions:
        if is_correct_pair_abbr_definition(abbr, definition):
            one_of_definitions_is_correct = True

            definition_words = definition.replace(u"-", u" ").split()
            definition_in_sentence = True
            for word in definition_words:
                if sentence.lower().find(word.lower()) == -1:
                    definition_in_sentence = False
            if definition_in_sentence:
                one_of_definitions_in_sentence = True
    return one_of_definitions_is_correct and one_of_definitions_in_sentence


def clear_data_with_sentence(data):
    FILE_WITH_NOT_USED_DATA = u"not_used"
    clear_data = []
    print u"Clear data:"
    with codecs.open(FILE_WITH_NOT_USED_DATA, u"w", u"utf8") as file:
        for i, (abbr, definition, sentence) in enumerate(data):
            if i % 100000 == 0:
                print i
            if is_correct_for_statistics(abbr,definition, sentence):
                clear_data.append((abbr, definition, sentence))
            else:
                file.write(abbr + u" def(" + definition + u") " + sentence + u"\n")
    print u"Cleared"
    return [(abbr, definition, sentence) for (abbr, definition, sentence) in data if
            is_correct_for_statistics(abbr,definition, sentence)]


def clear_data_with_sentence_and_many_definitions(data):
    FILE_WITH_NOT_USED_DATA = u"not_used"
    clear_data = []
    print u"Clear data:"
    with codecs.open(FILE_WITH_NOT_USED_DATA, u"w", u"utf8") as file:
        for i, (abbr, definitions, sentence) in enumerate(data):
            if i % 100000 == 0:
                print i
            if is_correct_for_statistics_with_many_definitions(abbr,definitions, sentence):
                clear_data.append((abbr, definitions, sentence))
            else:
                file.write(abbr + u" def(" + u" | ".join(definitions) + u") " + sentence + u"\n")
    print u"Cleared"
    return [(abbr, definition, sentence) for (abbr, definition, sentence) in data if
            is_correct_for_statistics_with_many_definitions(abbr,definition, sentence)]

def equal_definitions(definitions1, definitions2):

    definitions1_set = {stemmer.stem(word.lower()) for word in definitions1}
    definitions2_set = {stemmer.stem(word.lower()) for word in definitions2}
    if definitions1_set == definitions2_set:
        return CLEAR_EQUALS
    if distance(u" ".join([stemmer.stem(word.lower()) for word in definitions1]),
                u" ".join([stemmer.stem(word.lower()) for word in definitions2])) <= 4:
        return LEVENSHTAIN_EQUALS

    if (definitions1_set.union(definitions2_set) == definitions1_set
        or definitions1_set.union(definitions2_set) == definitions2_set):
        return SUBSET_EQUALS

    return NOT_EQUALS



def definition_in_definitions(definition, definitions):
    res = NOT_EQUALS
    definition_without_minus = []
    for word in definition:
        definition_without_minus += word.split(u"-")
    for example_definition in definitions:
        definition_words = [word for word in example_definition.replace(u"-", u" ").split() if word != u""]
        res = min(res, equal_definitions(definition_without_minus, definition_words))
    return res


def get_rules_priority(file_name):
    c = 0
    with open(file_name, 'r') as file:
        examples = json.loads(file.read())
    clear_examples = clear_data_with_sentence_and_many_definitions(examples)
    results = [0 for i in range(10)]
    for i, (abbr, definitions, sentence) in enumerate(clear_examples):
        abbr = abbr.lower()
        sentence = sentence.lower()
        if i % 100 == 0:
            print u"Tested " + unicode(i)

        abbr_pattern = abbr + r"s?"
        for definition in definitions:
            if definition == u"" or definition.lower() == abbr:
                continue
            words_without_punctuation = r"(?P<words>.{0,5}" + definition.lower() + r".{0,5})"
            if definition.lower() in sentence :
                c += 1
            rules_with_side = [words_without_punctuation + r"[\(\[\{]" + abbr_pattern + r"[\)\]\}]",
                               r"[\(\[\{]" + abbr_pattern + r"[\)\]\}]" + words_without_punctuation,
                               abbr_pattern + r"\s?[\(\[\{]" + words_without_punctuation + r"[\)\]\}]",
                               r"[\(\[\{]" + words_without_punctuation + r"[\)\]\}]\s?" + abbr_pattern,
                               words_without_punctuation + r"=\s?" + abbr_pattern,
                               abbr_pattern + r"\s?=" + words_without_punctuation,
                               words_without_punctuation + r",\s?or\s" + abbr_pattern,
                               abbr_pattern + r"\s?,\s?or" + words_without_punctuation,
                               abbr_pattern + r"\s(stands|short|acronym)" + words_without_punctuation,
                               words_without_punctuation + r",\s?" + abbr_pattern + r"\sfor short"
                     ]
            d = 0
            for j, rule in enumerate(rules_with_side):
                match = re.compile(rule).search(sentence)
                if match is not None:
                    if j == 1:
                        print sentence
                    results[j]+=1
                    d+=1

    print results
    print c
    return 0

def get_statistics_many_definitions2(file_name):
    results = defaultdict(lambda: defaultdict(lambda: 0))
    path_to_statistics = u"statistics_data/" + datetime.datetime.now().isoformat().replace(u':', u'-')

    try:
        os.makedirs(path_to_statistics)
    except:
        print u"Error! Cannot make a directory."

    with open(file_name, 'r') as file:
        examples = json.loads(file.read())
    with open(path_to_statistics + u"/not_found", 'w') as file:
        file.write(u"")
    with open(path_to_statistics + u"/not_equals", 'w') as file:
        file.write(u"")
    with open(path_to_statistics + u"/subset", 'w') as file:
        file.write(u"")
    with open(path_to_statistics + u"/lev", 'w') as file:
        file.write(u"")
    with open(path_to_statistics + u"/clear", 'w') as file:
        file.write(u"")
    with open(path_to_statistics + u"/not_in_sw", 'w') as file:
        file.write(u"")
    with open(path_to_statistics + u"/good_triple", 'w') as file:
        file.write(u"")
    with open(path_to_statistics + u"/bad_bad", 'w') as file:
        file.write(u"")

    not_found_to_json = []
    not_equals_to_json = []
    subset_to_json = []
    lev_to_json = []
    clear_to_json = []
    not_in_sw_to_json = []
    len_examples = len(examples)
    clear_examples = clear_data_with_sentence_and_many_definitions(examples)
    len_clear_examples = len(clear_examples)

    print unicode(len_clear_examples) + u"/" + unicode(len_examples)
    print u"Start testing"

    not_found_abbr = 0
    not_in_sw = 0
    clear_examples_group_by_sentence = defaultdict(lambda: dict())

    for abbr, definitions, sentence in clear_examples:
        clear_examples_group_by_sentence[sentence][abbr] = definitions

    for i, (sentence, test_abbrs_definitions) in enumerate(clear_examples_group_by_sentence.items()):
        if i % 100 == 0:
            print u"Tested " + unicode(i)
        abbrs_with_definition = get_abbr_with_definition(sentence)
        for abbr, definitions in test_abbrs_definitions.items():
            if abbr not in abbrs_with_definition:
                not_found_to_json.append((abbr, definitions, sentence))
                with codecs.open(u"not_found", 'a', u"utf8") as file:
                    file.write(abbr + u" " + sentence + u"\n")
                not_found_abbr += 1
                continue
            else:
                type_equals = definition_in_definitions(abbrs_with_definition[abbr], definitions)
                if type_equals == CLEAR_EQUALS:
                    file_name = u"clear"
                    file_name2 = u"good_triple"
                    clear_to_json.append((abbr, abbrs_with_definition[abbr], definitions, sentence))
                elif type_equals == LEVENSHTAIN_EQUALS:
                    file_name = u"lev"
                    file_name2 = u"good_triple"
                    lev_to_json.append((abbr, abbrs_with_definition[abbr], definitions, sentence))
                elif type_equals == SUBSET_EQUALS:
                    file_name = u"subset"
                    file_name2 = u"good_triple"
                    subset_to_json.append((abbr, abbrs_with_definition[abbr], definitions, sentence))
                elif type_equals == NOT_EQUALS:
                    file_name = u"not_equals"
                    file_name2 = u"bad_triple"
                    not_equals_to_json.append((abbr, abbrs_with_definition[abbr], definitions, sentence))

                results[len(abbr)][type_equals] += 1

                with codecs.open(path_to_statistics + u"/" + file_name, u'a', u"utf8") as file:
                    file.write(abbr + u" " + sentence + u"\n")
                    file.write(u"defSW        " + u" | ".join(definitions) + u"\n")
                    file.write(u"defSWABBRLIB " + u" ".join(abbrs_with_definition[abbr]) + u"\n")

                with codecs.open(path_to_statistics + u"/" + file_name2, u'a', u"utf8") as file:
                    file.write(unicode(abbrs_with_definition[abbr].distance)
                               + u" " + unicode(abbrs_with_definition[abbr].rule_number)
                               + u" " + unicode(abbrs_with_definition[abbr].layer) + u"\n")
                continue

        for abbr in abbrs_with_definition.keys():
            if abbr not in test_abbrs_definitions:
                not_in_sw_to_json.append((abbr, abbrs_with_definition[abbr], sentence))
                not_in_sw += 1

                with codecs.open(path_to_statistics + u"/" + u"not_in_sw", u'a', u"utf8") as file:
                    file.write(abbr + u" " + sentence + u"\n")
                    file.write(u"defSWABBRLIB " + u" ".join(abbrs_with_definition[abbr]) + u"\n")
    print u"End testing"
    print u"not in sw:" + unicode(not_in_sw)
    print u"not_found:" + unicode(not_found_abbr) + u" all:" + unicode(len_clear_examples) + u"\n"
    for i, cur_res in results.items():
        print i
        print u"\n"
        for j, count in cur_res.items():
            print j, u"res", count
    print results

    graphic_of_relationship(path_to_statistics)
    graphic_of_parameters(path_to_statistics)
    write_to_excel(path_to_statistics, results, len_clear_examples)

get_statistics_many_definitions2(u"notconfirmed.json")
#get_rules_priority(u"notconfirmed.json")

