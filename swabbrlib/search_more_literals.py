"""
Example of using get_possible_literals(abbr_with_definition, text):
We have a sentence: "fictional Darth Vader (fictional DV)".
abbr_with_definition: {u"DV": Definition([u"Darth", u"Vader"], start=10, end=20)}
text: u"fictional Darth Vader (fictional DV)"
return [Literal([u"fictional", u"Darth", u"Vader"], start=0, end=20])]
"""

import re
from swabbrlib import Sentence, IntersectingType

stop_symbols = {u'.', u',', u'!', u'?', u'{', u'}', u'(', u')', u'[', u']'}


class Literal(list):
    start = None
    end = None

    def __init__(self, list_, start, end):
        list.__init__(self, list_)
        self.start = start
        self.end = end


def first_word_after_index(index, text):
    word = u''
    text_len = len(text)
    cur_position = index + 1
    start = -1

    while cur_position < text_len and text[cur_position] not in stop_symbols:
        if text[cur_position].isalpha():
            if word == u'':
                start = cur_position
            word += text[cur_position]
        elif word != u'':
            cur_position += 1
            break
        cur_position += 1

    return Sentence(word, start, cur_position)


def first_word_before_index(index, text):
    word = u''
    position = index - 1
    end = -1

    while position >= 0 and text[position] not in stop_symbols:
        if text[position].isalpha():
            if word == u'':
                end = position + 1
            word += text[position]
        elif word != u'':
            position -= 1
            break
        position -= 1

    return Sentence(word[::-1], position + 1, end)


def get_equal_words_before_positions(first_position, second_position, text):
    words_before_first = []
    words_before_second = []
    current_word_before_first_position = first_word_before_index(first_position, text)
    current_word_before_second_position = first_word_before_index(second_position, text)

    while (current_word_before_first_position.text != u'' and
           current_word_before_first_position.text == current_word_before_second_position.text):
        words_before_first.append(current_word_before_first_position.text)
        words_before_second.append(current_word_before_second_position.text)

        first_position = current_word_before_first_position.start
        second_position = current_word_before_second_position.start

        current_word_before_first_position = first_word_before_index(first_position, text)
        current_word_before_second_position = first_word_before_index(second_position, text)

    words_before_first.reverse()
    words_before_second.reverse()

    return (Literal(words_before_first, first_position, None),
            Literal(words_before_second, second_position, None))


def get_equal_words_after_positions(first_position, second_position, text):
    words_after_first = []
    words_after_second = []
    current_word_after_first_position = first_word_after_index(first_position, text)
    current_word_after_second_position = first_word_after_index(second_position, text)

    while (current_word_after_first_position.text != u'' and
           current_word_after_first_position.text == current_word_after_second_position.text):
        words_after_first.append(current_word_after_first_position.text)
        words_after_second.append(current_word_after_second_position.text)

        first_position = current_word_after_first_position.end - 1
        second_position = current_word_after_second_position.end - 1

        current_word_after_first_position = first_word_after_index(first_position, text)
        current_word_after_second_position = first_word_after_index(second_position, text)

    return (Literal(words_after_first, None, first_position + 1),
            Literal(words_after_second, None, second_position + 1))


def get_possible_literal(abbr, definition, text):
    literal = Literal(definition, definition.start, definition.end)
    match = re.compile(r'\b' + abbr + r's?\b').search(text)

    if match is not None:
        start_abbr = match.start()
        end_abbr = match.end()

        words_before_abbr, words_before_definition = get_equal_words_before_positions(start_abbr, definition.start, text)
        if words_before_abbr is not []:
            literal = Literal(words_before_definition + literal, words_before_definition.start, literal.end)

        words_after_abbr, words_after_definition = get_equal_words_after_positions(end_abbr - 1, definition.end - 1,
                                                                                   text)
        if words_after_abbr is not []:
            literal = Literal(literal + words_after_definition, literal.start, words_after_definition.end)

        if words_before_abbr or words_after_abbr:
            return literal

    return None


def get_possible_literals(abbr_with_definition, text):
    more_precisely_literals = []
    for abbr, definition in abbr_with_definition.items():
        literal = get_possible_literal(abbr, definition, text)
        if literal is not None:
            more_precisely_literals.append(literal)

    return more_precisely_literals

