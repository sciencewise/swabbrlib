try:
    dict.iterkeys
except AttributeError:
    # Python 3
    def iterkeys(d):
        return iter(d.keys())
else:
    # Python 2
    def iterkeys(d):
        return d.iterkeys()
