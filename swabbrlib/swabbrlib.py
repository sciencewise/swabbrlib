# coding=utf-8
from __future__ import absolute_import

import re
import os
import json
import codecs
from collections import defaultdict

from swabbrlib.compatibility import iterkeys


MOST_COMMON_CP_RULE_COEFFICIENT = 20
ORDINARY_WORD_TYPE = u'o'
BLACKLIST_WORD_TYPE = u'b'
ABBR_PATTERN_LETTER_TYPE = u'l'
ABBR_PATTERN_NUMBER_TYPE = u'n'
STANDARD_RULES = None
STANDARD_RULES_FILE_NAME = u'standard_rules.json'
LEFT_SIDE = u'left'
RIGHT_SIDE = u'right'
abbr_pattern = re.compile(r'\b[A-Z][A-Za-z0-9]*[A-Z][A-Za-z0-9]*s?\b')


punctuation = r'\.?\+\:\[\]\(\)\{\}'
words_without_punctuation = r'(?P<words>[^' + punctuation + r']*)'
abbr_pattern_generic = r'(?P<abbr>\b[A-Z][A-Za-z0-9]*[A-Z][A-Za-z0-9]*s?\b)'


def get_rules_with_side(abbr):
    """
    It finds parts of the sentence where the candidate may exists.
    Rules:
    (1) definition (abbr)
    (2) (abbr) definition
    (3) abbr (definition)
    (4) (definition) abbr
    (5) definition = abbr
    (6) abbr = definition
    (7) abbr, or definition
    (8) definition, or abbr
    (9) abbr … stands/short/acronym … definition
    (10) definition, abbr for short
    (11) abbr - definition
    (12) definition - abbr
    """

    abbr_pattern_particular = r'(?P<abbr>{}s?)'.format(abbr)
    rules_with_side = [
        ([words_without_punctuation + r'[\(\[\{][^\(\[\{\)\]\}]*\b' + abbr_pattern + r'\b[^\(\[\{\)\]\}]*[\)\]\}]'
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], LEFT_SIDE),
        ([r'[\(\[\{][^\(\[\{\)\]\}]*\b' + abbr_pattern + r'\b[^\(\[\{\)\]\}]*[\)\]\}]' + words_without_punctuation
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], RIGHT_SIDE),
        ([abbr_pattern + r'\s*[\(\[\{](?P<words>[^\(\[\{\)\]\}]*)[\)\]\}]'
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], RIGHT_SIDE),
        ([r'[\(\[\{](?P<words>[^\(\[\{\)\]\}]*)[\)\]\}]\s*' + abbr_pattern
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], LEFT_SIDE),
        ([words_without_punctuation + r'=\s?' + abbr_pattern
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], LEFT_SIDE),
        ([abbr_pattern + r'\s?=' + words_without_punctuation
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], RIGHT_SIDE),
        ([words_without_punctuation + r',?\s?or\s' + abbr_pattern
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], LEFT_SIDE),
        ([abbr_pattern + r'\s?,?\s?or' + words_without_punctuation
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], RIGHT_SIDE),
        ([abbr_pattern + r'\s(stands|short|acronym)' + words_without_punctuation
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], RIGHT_SIDE),
        ([words_without_punctuation + r',\s?' + abbr_pattern + r'\sfor short'
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], LEFT_SIDE),
        ([abbr_pattern + r'\s-+\s' + words_without_punctuation
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], RIGHT_SIDE),
        ([words_without_punctuation + r'\s-+\s' + abbr_pattern
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], LEFT_SIDE),
        ([abbr_pattern + r'(\s+\w+){1,3}\s*[\(\[\{](?P<words>[^\(\[\{\)\]\}]*)[\)\]\}]'
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], RIGHT_SIDE),
        ([r'[\(\[\{](?P<words>[^\(\[\{\)\]\}]*)[\)\]\}](\s+\w+){1,3}\s*' + abbr_pattern
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], LEFT_SIDE),
        ([words_without_punctuation + r',\s?(a|the)?\s?' + abbr_pattern
          for abbr_pattern in (abbr_pattern_generic, abbr_pattern_particular)], LEFT_SIDE),
    ]

    return rules_with_side


BLACK_LIST_FILE_NAME = os.path.join(os.path.dirname(__file__), u'blacklist')
with codecs.open(BLACK_LIST_FILE_NAME, u'r', u'utf8') as document:
    BLACK_LIST = set(document.read().split(u'\n'))


class RulePatternTypes(object):
    FIRST_LETTER = u'f'
    MIDDLE_LETTER = u'm'
    LAST_LETTER = u'l'
    NOT_FOUND = u'n'


class IntersectingType(object):
    IS_LITERAL = 5
    POSSIBLE_LITERAL = 4
    LITERAL_IN_CANDIDATE = 3
    CANDIDATE_IN_LITERAL = 2
    INTERSECTION_WITH_LITERAL = 1
    NOT_LITERAL = 0


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class CandidatePlaceRuleScores(object):
    __metaclass__ = Singleton
    scores = defaultdict(lambda: 0)
    most_common_rule = None

    def get_rule_score(self, number_of_rule):
        if self.most_common_rule is None:
            self.calculate_most_common_rule()
        if number_of_rule == self.most_common_rule:
            return 1
        else:
            return 0

    def clear(self):
        self.scores = defaultdict(lambda: 0)
        self.most_common_rule = None

    def add_rule_intermission(self, number_of_rule):
        self.scores[number_of_rule] += 1

    def calculate_most_common_rule(self):
        self.most_common_rule = max(iterkeys(self.scores), key=(lambda key: self.scores[key]))


class CandidateScore:
    """
    Help to get the best score for candidate and abbreviation. Find the best rule.
    Scores are compered with each other by:
    1) The best rule is in standard rules.
    2) Distance.
    3) Layer.
    4) Cnt of words from blacklist.
    """
    abbr = None
    candidate = []
    candidate_type = None
    best_rule = []
    is_standard_rule = False
    intersecting_rank = None
    distance_to_abbr = None
    candidate_place_rank = None
    count_hyphen_in_candidate = 0

    def __init__(self, abbr, candidate, candidate_type, distance, candidate_place_score, intersecting_rank):
        self.abbr = abbr
        self.candidate = candidate
        self.count_hyphen_in_candidate = sum(1 for word in self.candidate for latter in word.text if latter == u'-')
        self.candidate_type = candidate_type
        self.distance_to_abbr = distance
        self.best_rule, self.is_standard_rule = get_best_rule(self.abbr, self.candidate, self.candidate_type)
        self.candidate_place_rank = candidate_place_score
        self.intersecting_rank = intersecting_rank

    def get_candidate_layer(self):
        count_words_from_blacklist = count_candidate_words_from_blacklist(self.candidate_type)

        if len(self.abbr) == len(self.candidate) + self.count_hyphen_in_candidate:
            return 1
        if len(self.abbr) == len(self.candidate) + self.count_hyphen_in_candidate - count_words_from_blacklist:
            return 2
        if len(self.abbr) < len(self.candidate) + self.count_hyphen_in_candidate:
            return 3
        if len(self.abbr) > len(self.candidate) + self.count_hyphen_in_candidate:
            return 4


class Sentence:
    text = None
    start = None
    end = None

    def __init__(self, text, start, end):
        self.start = start
        self.end = end
        self.text = text


class Definition(list):
    type = None
    start = None
    end = None
    distance = None
    rule_number = None
    layer = None

    def __init__(self, l, type, start, end, distance, rule_number, layer):
        list.__init__(self, l)
        self.type = type
        self.start = start
        self.end = end
        self.distance = distance
        self.rule_number = rule_number
        self.layer = layer


def get_candidate_place_score(candidate_score):
    return (
        candidate_score.candidate_place_rank -
        MOST_COMMON_CP_RULE_COEFFICIENT *
        CandidatePlaceRuleScores().get_rule_score(candidate_score.candidate_place_rank))


def count_candidate_words_from_blacklist(candidate_words_type):
    return sum(1 for word_type in candidate_words_type if word_type == BLACKLIST_WORD_TYPE)


def get_sentence(text, index):
    """
    It returns a sentence which contains an element with index equals to index-parameter.
    """
    end_of_sentence_punctuation = {u'.', u'!', u'?'}
    sentence_start = index - 1
    sentence_end = index + 1

    while sentence_start >= 0 and text[sentence_start] not in end_of_sentence_punctuation:
        sentence_start -= 1
    sentence_start += 1

    while sentence_end < len(text) and text[sentence_end] not in end_of_sentence_punctuation:
        sentence_end += 1

    return Sentence(text[sentence_start: sentence_end], sentence_start, sentence_end)


def get_abbr_with_first_sentence(text):
    """
    It finds an abbreviation and the first sentence which contains this abbreviation.
    """
    from_abbr_to_first_index = {}
    from_abbr_to_first_sentence = {}

    for match in abbr_pattern.finditer(text):
        abbr = match.group()

        if abbr[-1] == u's':
            abbr = abbr[:-1]

        if abbr in from_abbr_to_first_index:
            from_abbr_to_first_index[abbr] = min(from_abbr_to_first_index[abbr], match.start())
        else:
            from_abbr_to_first_index[abbr] = match.start()

    for abbr, index in from_abbr_to_first_index.items():
        from_abbr_to_first_sentence[abbr] = get_sentence(text, index)

    return from_abbr_to_first_sentence


def find_candidates_place_via_cp_rules(abbr, sentence):
    rules_with_side = get_rules_with_side(abbr)

    found_candidate_places = []

    for (rule_generic, rule_particular), side in rules_with_side:
        matched = False

        # to make re.compile faster, first we use generic rule of that side
        # (it will be cached by RE)
        # if definition and abbr found - we check if input abbr is equal to matched abbr
        # because in sentence with two abbrs only first abbr will be matched
        # if input abbr is not equal to matched abbr - we use regexp with hardcoded abbr group
        for rule in [rule_generic, rule_particular]:
            match = re.compile(rule).search(sentence.text)
            if match is None:
                break
            elif match.group('abbr') == abbr or match.group('abbr') == abbr + 's':
                found_candidate_places.append((
                    Sentence(match.group(u'words'),
                             match.start(u'words') + sentence.start,
                             match.end(u'words') + sentence.start),
                    side))
                matched = True
                break

        if not matched:
            found_candidate_places.append((None, side))

    return found_candidate_places


def get_black_list():
    BLACK_LIST_FILE_NAME = os.path.join(os.path.dirname(__file__), u'blacklist')

    with codecs.open(BLACK_LIST_FILE_NAME, u'r', u'utf8') as document:
        black_list = document.read().split(u'\n')

    return set(black_list)


def get_words_positions_in_sentence(sentence, start_sentence):
    location = -1
    words = sentence.split()
    words_with_positions = []

    for word in words:
        location = sentence.index(word, location + 1)
        words_with_positions.append(Sentence(word, location + start_sentence, location + len(word) + start_sentence))

    return words_with_positions


def find_candidates_with_distance(abbr, sentence_part_const, side, candidate_place_score):
    """
    For each subsequence it returns the distance to abbreviation and subsequence itself where the first
    word begins with the first letter of abbreviation and the last word contains the last letter of
    abbreviation.
    """
    black_list = get_black_list()
    sentence_part = re.sub(r'((?<=\W)[-\'])|([-\'](?=\W))|([^\w\'-])', u' ', sentence_part_const.text)
    words = get_words_positions_in_sentence(sentence_part, sentence_part_const.start)
    words_type = [BLACKLIST_WORD_TYPE if word.text in black_list else ORDINARY_WORD_TYPE for word in words]
    candidates = []

    for def_start_index in range(len(words)):
        if words_type[def_start_index] == BLACKLIST_WORD_TYPE:
            continue
        if words[def_start_index].text[0].lower() != abbr[0].lower():
            continue

        for def_end_index in range(def_start_index, len(words)):
            if def_start_index == def_end_index and abbr == words[def_start_index].text:
                continue
            if abbr[-1].lower() not in words[def_end_index].text.lower():
                continue
            if def_end_index - def_start_index + 1 > min(len(abbr) + 5, len(abbr) * 2):
                continue
            if side == LEFT_SIDE:
                dist = len(words) - def_end_index - 1
            else:
                dist = def_start_index
            candidates.append((words[def_start_index:def_end_index + 1], words_type[def_start_index:def_end_index + 1], dist, candidate_place_score))

    return candidates


def get_abbr_pattern(abbr):
    return [ABBR_PATTERN_NUMBER_TYPE if letter.isdigit() else ABBR_PATTERN_LETTER_TYPE for letter in abbr]


def recursive_rules_generation(abbr, letter_number, words, word_number, position_in_word, cur_rule, rules):
    """
    Generate all possible rules that appropriate for abbreviation and the candidate.
    Details:
    Decrypt letters starting with letter under the number letter_number to the last letter
    in abbreviation. It uses words beginning with word under the number word_number + 1 and
    uses words[word_number][position_in_word:].
    """
    if word_number >= len(words):
        return

    if letter_number >= len(abbr):
        return

    if letter_number == len(abbr) - 1:
        if (position_in_word == 0 or word_number < len(words) - 1) and words[-1][0] == abbr[- 1]:
            rules.append(cur_rule + [(len(words) - 1, RulePatternTypes.FIRST_LETTER)])

        if word_number < len(words) - 1:
            position_in_last_word = 0
        else:
            position_in_last_word = position_in_word

        if words[-1].find(abbr[-1], max(1, position_in_last_word), len(words[-1]) - 1) != -1:
            rules.append(cur_rule + [(len(words) - 1, RulePatternTypes.MIDDLE_LETTER)])

        if abbr[-1] == words[-1][-1]:
            rules.append(cur_rule + [(len(words) - 1, RulePatternTypes.LAST_LETTER)])
        return

    if letter_number < len(abbr) - 1:
        if words[word_number].find(abbr[letter_number], position_in_word) != -1:
            abbr_letter_position = words[word_number].find(abbr[letter_number], position_in_word)

            if abbr_letter_position == 0:
                cur_rule.append((word_number, RulePatternTypes.FIRST_LETTER))
                recursive_rules_generation(abbr, letter_number + 1, words, word_number,
                                           abbr_letter_position + 1, cur_rule, rules)
                cur_rule.pop()

            elif 0 < abbr_letter_position < len(words[word_number]) - 1:
                cur_rule.append((word_number, RulePatternTypes.MIDDLE_LETTER))
                recursive_rules_generation(abbr, letter_number + 1, words, word_number,
                                           abbr_letter_position + 1, cur_rule, rules)
                cur_rule.pop()

            elif abbr_letter_position == len(words[word_number]) - 1:
                cur_rule.append((word_number, RulePatternTypes.LAST_LETTER))
                recursive_rules_generation(abbr, letter_number + 1, words, word_number + 1,
                                           0, cur_rule, rules)
                cur_rule.pop()

        recursive_rules_generation(abbr, letter_number, words, word_number + 1, 0, cur_rule, rules)


def generate_formation_rules(abbr, words_with_position, words_type):
    words = [word.text for word in words_with_position]
    if abbr in words:
        return []
    if abbr[0].lower() != words[0][0].lower():
        return []
    if words_type[0] == BLACKLIST_WORD_TYPE or words_type[-1] == BLACKLIST_WORD_TYPE:
        return []

    lower_abbr = abbr.lower()
    lower_words = [word.lower() for word in words]
    rules = []
    cur_rule = [(0, RulePatternTypes.FIRST_LETTER)]

    recursive_rules_generation(lower_abbr, 1, lower_words, 0, 1, cur_rule, rules)
    # sort rules by how many first letters of definition form abbreviation
    rules.sort(key=lambda x: sum(1 for _, rule_pattern_type in x
                                 if rule_pattern_type == RulePatternTypes.FIRST_LETTER),
               reverse=True)

    return rules


def get_standard_rule(abbr_pattern, candidate_words_type):
    with codecs.open(os.path.join(os.path.dirname(__file__), STANDARD_RULES_FILE_NAME), u'r', u'utf8') as document:
        global STANDARD_RULES
        STANDARD_RULES = json.loads(document.read())
    key = ''.join(abbr_pattern) + u':' + ''.join(candidate_words_type)

    return STANDARD_RULES[key] if key in STANDARD_RULES else []


def get_best_rule(abbr, candidate_words, candidate_words_type):
    rules = generate_formation_rules(abbr, candidate_words, candidate_words_type)
    rules_set = {tuple(rule) for rule in rules}
    standard_rules = {tuple([tuple(pair) for pair in rule])
                      for rule in get_standard_rule(get_abbr_pattern(abbr), candidate_words_type)}
    rules_intersection = rules_set.intersection(standard_rules)

    if len(rules_intersection) > 0:
        return rules_intersection.pop(), True
    elif len(rules) == 0:
        return None, None
    else:
        return rules[0], False


def rank_intersect(intercept1, intercept2):
    if intercept2[0] == intercept1[0] and intercept2[1] == intercept1[1]:
        return IntersectingType.IS_LITERAL

    if intercept2[0] <= intercept1[0] <= intercept2[1] and intercept2[0] <= intercept1[1] <= intercept2[1]:
        return IntersectingType.CANDIDATE_IN_LITERAL

    if intercept1[0] <= intercept2[0] <= intercept1[1] and intercept1[0] <= intercept2[1] <= intercept1[1]:
        return IntersectingType.LITERAL_IN_CANDIDATE

    if ((intercept2[0] <= intercept1[0] <= intercept2[1]) or
            (intercept2[0] <= intercept1[1] <= intercept2[1]) or
            (intercept1[0] <= intercept2[0] <= intercept1[1]) or
            (intercept1[0] <= intercept2[1] <= intercept1[1])):
        return IntersectingType.INTERSECTION_WITH_LITERAL

    return IntersectingType.NOT_LITERAL


def intersecting_rank(example_intercept, intercepts):
    if intercepts is None:
        return 0
    result = 0
    for intercept in intercepts:
        result = max(result, rank_intersect(example_intercept, intercept))

    return result


def get_abbr_with_definition(text, terms=None):
    """
    Find all abbreviations with first sentences. For each abbreviation it builds the list of candidates. For each
    candidate it calculates the CandidateScore. The definition of abbreviation is candidate with the best
    CandidateScore.
    """
    cp_rules_score = CandidatePlaceRuleScores()
    cp_rules_score.clear()

    from_abbr_to_definition = {}
    from_abbr_to_first_sentence = get_abbr_with_first_sentence(text)
    from_abbr_to_candidate_score = {}

    if terms is not None:
        terms_positions = [(term.start, term.end) for term in terms]
    else:
        terms_positions = []

    for abbr, sentence in from_abbr_to_first_sentence.items():
        found_candidates_place = find_candidates_place_via_cp_rules(abbr, sentence)
        candidates = []
        candidate_scores = []

        for candidate_place_rank, (candidates_place, side) in enumerate(found_candidates_place):
            if candidates_place is not None:
                candidates += (find_candidates_with_distance(abbr, candidates_place, side, candidate_place_rank))

        used_candidate_place_ranks = set()

        for candidate_words_with_positions, candidate_words_type, dist, candidate_place_rank in candidates:
            candidate_score = CandidateScore(abbr, candidate_words_with_positions, candidate_words_type, dist,
                                             candidate_place_rank,
                                             intersecting_rank((candidate_words_with_positions[0].start,
                                                                candidate_words_with_positions[-1].end),
                                                               terms_positions))

            if candidate_score.best_rule is not None:
                candidate_scores.append(candidate_score)
                if candidate_place_rank not in used_candidate_place_ranks:
                    cp_rules_score = CandidatePlaceRuleScores()
                    cp_rules_score.add_rule_intermission(candidate_place_rank)
                    used_candidate_place_ranks.add(candidate_place_rank)

        from_abbr_to_candidate_score[abbr] = candidate_scores

    for abbr in from_abbr_to_first_sentence.keys():
        candidate_scores = from_abbr_to_candidate_score[abbr]
        candidate_scores.sort(key=lambda x: (x.intersecting_rank,
                                             -get_candidate_place_score(x),
                                             x.is_standard_rule,
                                             -x.distance_to_abbr,
                                             -x.get_candidate_layer(),
                                             -count_candidate_words_from_blacklist(x.candidate_type)),
                              reverse=True)
        if len(candidate_scores) != 0:
            from_abbr_to_definition[abbr] = Definition([word.text for word in candidate_scores[0].candidate],
                                                       candidate_scores[0].intersecting_rank,
                                                       start=candidate_scores[0].candidate[0].start,
                                                       end=candidate_scores[0].candidate[-1].end,
                                                       distance=candidate_scores[0].distance_to_abbr,
                                                       rule_number=candidate_scores[0].candidate_place_rank,
                                                       layer=candidate_scores[0].get_candidate_layer())

    return from_abbr_to_definition
