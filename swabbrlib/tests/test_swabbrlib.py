import functools
import nose
from unittest import TestCase
import codecs
from swabbrlib.swabbrlib import (get_abbr_with_first_sentence, get_sentence, find_candidates_place_via_cp_rules,
                                 find_candidates_with_distance, get_abbr_pattern, generate_formation_rules, get_abbr_with_definition, LEFT_SIDE,
                                 RIGHT_SIDE, Sentence, IntersectingType)


def expected_failure(test):
    @functools.wraps(test)
    def inner(*args, **kwargs):
        try:
            test(*args, **kwargs)
        except Exception:
            raise nose.SkipTest
        else:
            raise AssertionError('Failure expected')
    return inner


class Term:
    start = None
    end = None

    def __init__(self, start, end):
        self.start = start
        self.end = end


def dict_with_sentence_to_dict_with_unicode(with_sentence):
    with_unicode = {}
    for key, sentence in with_sentence.items():
        with_unicode[key] = sentence.text
    return with_unicode


class TestGet_abbr_with_first_sentence(TestCase):
    def test_duplicates(self):
        text = u"First sentence with DM. Second sentence with DM!!!"
        self.assertEqual(dict_with_sentence_to_dict_with_unicode(get_abbr_with_first_sentence(text)),
                         {u"DM": u"First sentence with DM"})

    def test_all_abbrs(self):
        text = u"First sentence with DM. Second sentence with MDD!!!"
        self.assertEqual(dict_with_sentence_to_dict_with_unicode(get_abbr_with_first_sentence(text)),
                         {u"DM": u"First sentence with DM", u"MDD": u" Second sentence with MDD"})

    def test_abbrs_in_one_sentence(self):
        text = u"First sentence with DM and with SSH."
        self.assertEqual(dict_with_sentence_to_dict_with_unicode(get_abbr_with_first_sentence(text)),
                         {u"DM": u"First sentence with DM and with SSH",
                          u"SSH": u"First sentence with DM and with SSH"})

    def test_abbrs_in_brackets(self):
        text = u"First sentence with (DM) and with [SSH] and with {TDD}"
        self.assertEqual(dict_with_sentence_to_dict_with_unicode(get_abbr_with_first_sentence(text)),
                         {u"DM": u"First sentence with (DM) and with [SSH] and with {TDD}",
                          u"SSH": u"First sentence with (DM) and with [SSH] and with {TDD}",
                          u"TDD": u"First sentence with (DM) and with [SSH] and with {TDD}"})

    def test_ending_with_s(self):
        text = u"First sentence with DMs"
        self.assertEqual(dict_with_sentence_to_dict_with_unicode(get_abbr_with_first_sentence(text)),
                         {u"DM": u"First sentence with DMs"})

    def test_abbrs_with_numbers(self):
        text = u"First sentence with D2M"
        self.assertEqual(dict_with_sentence_to_dict_with_unicode(get_abbr_with_first_sentence(text)),
                         {u"D2M": u"First sentence with D2M"})


class TestGet_abbr_with_definition(TestCase):
    def test_get_abbr_with_definition(self):
        text = u"the so-called Elitzur-Vaidman (EV) bomb [aka interaction free measurement (IFM)]"
        self.assertEqual(get_abbr_with_definition(text), {u"EV": [u"Elitzur-Vaidman"],
                                                          u"IFM": [u"interaction", u"free", u"measurement"]})

    @expected_failure
    def test_get_abbr_with_definition_with_used_position(self):
        text = u"Elements Vandam Elitzur-Vaidman (EV)]"
        self.assertEqual(get_abbr_with_definition(text), {u"EV": [u"Elitzur-Vaidman"]})

    def test_get_abbr_with_definition2(self):
        text = u"(PARTHENOPE) Public Algorithm Evaluating the Nucleosinthesis of Primordial Elements."
        self.assertEqual(get_abbr_with_definition(text), {u'PARTHENOPE':
                                                              [u'Public', u'Algorithm', u'Evaluating',
                                                               u'the', u'Nucleosinthesis', u'of',
                                                               u'Primordial', u'Elements']})

    def test_get_abbr_with_definition_with_small_latter(self):
        text = u"(PArthENoPE) Public Algorithm Evaluating the Nucleosinthesis of Primordial Elements."
        self.assertEqual(get_abbr_with_definition(text), {u'PArthENoPE':
                                                              [u'Public', u'Algorithm', u'Evaluating',
                                                               u'the', u'Nucleosinthesis', u'of',
                                                               u'Primordial', u'Elements']})

    @expected_failure
    def test_get_abbr_with_definition4(self):
        text = u"Distance scale and variable stars in Local Group Galaxies: LMC and Fornax.We briefly review our " \
               u"photometric and spectroscopic study of RR Lyrae variable stars in the bar of the Large Magellanic " \
               u"Cloud (LMC), that allowed us to reconcile the so-called 'short' and 'long' distance moduli of the LMC " \
               u"on the value mod_LMC=18.51 +/- 0.085 mag."
        self.assertEqual(get_abbr_with_definition(text), {u"LMC": [u"Large", u"Magellanic", u"Cloud"]})

    def test_get_abbr_with_definition5(self):
        text = u"was derived in the case of pure 2d gravity (the  model corresponding to ) using a completely different" \
               u" approach to quantum gravity called CDT (causal dynamical triangulations)  and the uniformization" \
               u" transformation relating the CDT boundary "
        self.assertEqual(get_abbr_with_definition(text), {u"CDT": [u"causal", u"dynamical", u"triangulations"]})

    def test_get_abbr_with_definition_in_short_text1(self):
        with codecs.open(u"swabbrlib/tests/short_text1", u"r", u"utf8") as file:
            text = file.read()
        self.assertEqual(get_abbr_with_definition(text), {u'DM': [u'dark', u'matter'],
                                                          u'SM': [u'Standard', u'Model'],
                                                          u'WIMP': [u'weakly', u'interacting', u'massive', u'particle']})

    def test_get_abbr_with_definition_in_short_text2(self):
        with codecs.open(u"swabbrlib/tests/short_text2", u"r", u"utf8") as file:
            text = file.read()
        self.assertEqual(get_abbr_with_definition(text), {u'BCG': [u'brightest', u'cluster', u'galaxies'],
                                                          u'AGN': [u'active', u'galactic', u'nuclei'],
                                                          u'IMF': [u'initial', u'mass', u'functions']})

    def test_get_abbr_with_definition_with_terms(self):
        text = u"Dark Matter (DM) Dart Maider"
        terms = [Term(0, len(u"Dark Matter"))]
        self.assertEqual(get_abbr_with_definition(text, terms), {u"DM": [u"Dark", u"Matter"]})

        terms = [Term(len(u"Dark Matter (DM) "), len(u"Dark Matter (DM) Dart Maider"))]
        self.assertEqual(get_abbr_with_definition(text, terms), {u"DM": [u"Dart", u"Maider"]})
        self.assertEqual(get_abbr_with_definition(text, terms)[u"DM"].type, IntersectingType.IS_LITERAL)

    def test_get_abbr_with_definition_not_full(self):
        text = u"public transport networks (PT networks)"
        self.assertEqual(get_abbr_with_definition(text), {u"PT": [u"public", u"transport"]})

    def test_get_abbr_with_definition_not_full2(self):
        text = u"PT networks (public transport networks)"
        self.assertEqual(get_abbr_with_definition(text), {u"PT": [u"public", u"transport"]})

    def test_get_abbr_with_definition_not_full3(self):
        text = u" public transport networks (or PTNs)"
        self.assertEqual(get_abbr_with_definition(text), {u"PTN": [u"public", u"transport", u"networks"]})

    def test_get_abbr_with_definition_not_full4(self):
        text = u" ABC concept (ABC)"
        self.assertEqual(get_abbr_with_definition(text), {})


class TestGet_sentence(TestCase):
    def test_first_sentence(self):
        text = u"First sentence. Second sentence! Third sentence"
        index = 5
        sentence = get_sentence(text, index)
        self.assertEqual(u"First sentence", sentence.text)
        self.assertEqual(len(u""), sentence.start)
        self.assertEqual(len(u"First sentence"), sentence.end)

    def test_middle_sentence(self):
        text = u"First sentence. Second sentence! Third sentence"
        index = 20
        sentence = get_sentence(text, index)
        self.assertEqual(u" Second sentence", sentence.text)
        self.assertEqual(len(u"First sentence."), sentence.start)
        self.assertEqual(len(u"First sentence. Second sentence"), sentence.end)

    def test_last_sentence(self):
        text = u"First sentence. Second sentence! Third sentence"
        index = 35
        sentence = get_sentence(text, index)
        self.assertEqual(u" Third sentence", sentence.text)
        self.assertEqual(len(u"First sentence. Second sentence!"), sentence.start)
        self.assertEqual(len(u"First sentence. Second sentence! Third sentence"), sentence.end)


class TestFind_candidates_place_via_syntactic_cues(TestCase):
    def test_first_rule(self):
        text = u"First sentence with (AMD)"
        sentence = Sentence(text, 0, 0)
        tested_sentence, side = find_candidates_place_via_cp_rules(u"AMD", sentence)[0]
        self.assertEqual(side, LEFT_SIDE)
        self.assertEqual(tested_sentence.text, u"First sentence with ")

    def test_second_rule(self):
        text = u"(DM) is dark matter"
        sentence = Sentence(text, 0, 0)
        tested_sentence, side = find_candidates_place_via_cp_rules(u"DM", sentence)[1]
        self.assertEqual(side, RIGHT_SIDE)
        self.assertEqual(tested_sentence.text, u" is dark matter")

    def test_third_rule(self):
        text = u"DM (dark matter)"
        sentence = Sentence(text, 0, 0)
        tested_sentence, side = find_candidates_place_via_cp_rules(u"DM", sentence)[2]
        self.assertEqual(side, RIGHT_SIDE)
        self.assertEqual(tested_sentence.text, u"dark matter")

    def test_fourth_rule(self):
        text = u"(dark matter) DM"
        sentence = Sentence(text, 0, 0)
        tested_sentence, side = find_candidates_place_via_cp_rules(u"DM", sentence)[3]
        self.assertEqual(side, LEFT_SIDE)
        self.assertEqual(tested_sentence.text, u"dark matter")

    def test_fifth_rule(self):
        text = u"dark matter = DM"
        sentence = Sentence(text, 0, 0)
        tested_sentence, side = find_candidates_place_via_cp_rules(u"DM", sentence)[4]
        self.assertEqual(side, LEFT_SIDE)
        self.assertEqual(tested_sentence.text, u"dark matter ")

    def test_sixth_rule(self):
        text = u"DM = dark matter"
        sentence = Sentence(text, 0, 0)
        tested_sentence, side = find_candidates_place_via_cp_rules(u"DM", sentence)[5]
        self.assertEqual(side, RIGHT_SIDE)
        self.assertEqual(tested_sentence.text, u" dark matter")

    def test_seventh_rule(self):
        text = u"dark matter, or DM"
        sentence = Sentence(text, 0, 0)
        tested_sentence, side = find_candidates_place_via_cp_rules(u"DM", sentence)[6]
        self.assertEqual(side, LEFT_SIDE)
        self.assertEqual(tested_sentence.text, u"dark matter, ")

    def test_eighth_rule(self):
        text = u"DM, or dark matter"
        sentence = Sentence(text, 0, 0)
        tested_sentence, side = find_candidates_place_via_cp_rules(u"DM", sentence)[7]
        self.assertEqual(side, RIGHT_SIDE)
        self.assertEqual(tested_sentence.text, u" dark matter")

    def test_ninth_rule(self):
        text = u"DM stands dark matter"
        sentence = Sentence(text, 0, 0)
        tested_sentence, side = find_candidates_place_via_cp_rules(u"DM", sentence)[8]
        self.assertEqual(side, RIGHT_SIDE)
        self.assertEqual(tested_sentence.text, u" dark matter")

    def test_tenth_rule(self):
        text = u"dark matter, DM for short"
        sentence = Sentence(text, 0, 0)
        tested_sentence, side = find_candidates_place_via_cp_rules(u"DM", sentence)[9]
        self.assertEqual(side, LEFT_SIDE)
        self.assertEqual(tested_sentence.text, u"dark matter")


class TestFind_candidates(TestCase):
    def test_common_meaning(self):
        text = u"Dark matter is above"
        sentence = Sentence(text, 0, 0)
        candidate_place_rank = 0
        candidates = find_candidates_with_distance(u"DM", sentence, RIGHT_SIDE, candidate_place_rank)

        self.assertEqual(1, len(candidates))
        self.assertEqual([word.text for word in candidates[0][0]], [u"Dark", u"matter"])
        self.assertEqual(candidates[0][1:], ([u'o', u'o'], 0, candidate_place_rank))


class TestGet_abbr_pattern(TestCase):
    def test_get_abbr_pattern(self):
        abbr = u"DM5"
        self.assertEqual(get_abbr_pattern(abbr), [u"l", u"l", u"n"])


class TestGenerate_formation_rules(TestCase):
    def test_first_letter(self):
        abbr = u"DM"
        words = [Sentence(u"Dark", 0, 0), Sentence(u"Matter", 0, 0)]
        self.assertEqual(generate_formation_rules(abbr, words, [u"o", u"o"]), [[(0, u"f"), (1, u"f")]])

    def test_middle_letter(self):
        abbr = u"DMA"
        words = [Sentence(u"Damp", 0, 0), Sentence(u"Amp", 0, 0)]
        self.assertEqual(generate_formation_rules(abbr, words, [u"o", u"o"]), [[(0, u"f"), (0, u"m"), (1, u"f")]])

    def test_two_variants(self):
        abbr = u"DA"
        words = [Sentence(u"Dark", 0, 0), Sentence(u"mamba", 0, 0)]
        self.assertEqual(generate_formation_rules(abbr, words, [u"o", u"o"]), [[(0, u"f"), (1, u"m")], [(0, u"f"), (1, u"l")]])
