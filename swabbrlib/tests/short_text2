We perform a weak-lensing study of the nearby cool-core galaxy clusters, Hydra A
(z = 0.0538) and A478 (z = 0.0881), of which brightest cluster galaxies (BCGs) host
powerful activities of active galactic nuclei (AGNs). For each cluster, the observed
tangential shear profile is well described either by a single Navarro–Frenk–White model
or a two-component model including the BCG as an unresolved point mass. For A478,
we determine the BCG and its host-halo masses from a joint fit to weak-lensing and
stellar photometry measurements. We find that the choice of initial mass functions
(IMFs) can introduce a factor of two uncertainty in the BCG mass, whereas the BCG
host halo mass is well constrained by data. We perform a joint analysis of weaklensing
and stellar kinematics data available for the Hydra A cluster, which allows
us to constrain the central mass profile without assuming specific IMFs. We find
that the central mass profile (r < 300 kpc) determined from the joint analysis is in
excellent agreement with those from independent measurements, including dynamical
masses estimated from the cold gas disk component, X-ray hydrostatic total mass
estimates, and the central stellar mass estimated based on the Salpeter IMF. The
observed dark-matter fraction around the BCG for Hydra A is found to be smaller
than those predicted by adiabatic contraction models, suggesting the importance of
other physical processes, such as the the AGN feedback and/or dissipationless mergers.